# Where did I Park

WhereDidIPark is able to save the parking location of a user‘s car and bike. The current location is fetched through the Google Maps API, a picture and optional note can be attached.
<br/><br/>



06/2016 | Android Smartphones



Entry for Bike |  New Entry for Bike | Saved up Car
:-------------------------:|:-------------------------:|:----:
<img src="images/park1.png" alt="Bike-Entry" width="200px">  |  <img src="images/park3.png" alt="New Bike-Entry" width="200px"> | <img src="images/park2.png" alt="Car-Entry" width="200px">
